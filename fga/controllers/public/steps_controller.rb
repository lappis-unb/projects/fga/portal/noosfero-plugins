class StepsController < PublicController

  no_design_blocks

  def tcc_steps
    @submition_phase = params[:submition_phase]
    @google_forms = params[:google_forms]
    @pdf_forms = params[:pdf_forms]
    @docx_forms = params[:docx_forms]
    @odt_forms = params[:odt_forms]
  end

  def login
    store_location(request.referer) unless params[:return_to] or session[:return_to]

    return unless request.post?

    begin
      self.current_user ||= User.authenticate(params[:user][:login], params[:user][:password], environment) if params[:user]
    rescue User::UserNotActivated => e
      session[:notice] = e.message
      return
    end
    if logged_in?
      check_join_in_community(self.current_user)

      if params[:remember_me] == "1"
        self.current_user.remember_me
        cookies[:auth_token] = {value: self.current_user.remember_token, expires: self.current_user.remember_token_expires_at}
      end

      session[:notice] = _("Logged in successfully")
    else
      session[:notice] = _('Incorrect username or password')
    end

    redirect_to :controller => :steps , :action => :tcc_steps, :submition_phase => params[:submition_phase] ,
    :pdf_forms => params[:pdf_forms], :google_forms => params[:google_forms], :odt_forms => params[:odt_forms], :docx_forms => params[:docx_forms]

  end

  def check_join_in_community(user)
    profile_to_join = session[:join]
    unless profile_to_join.blank?
     environment.profiles.find_by(identifier: profile_to_join).add_member(user.person)
     session.delete(:join)
    end
  end


  def render_upload_files
    @tcc_community = Community.find_by(identifier: "tcc")

    unless @tcc_community.in_social_circle?(current_user.person)
      @tcc_community.add_member(current_user.person)
    end

    course_name = params[:course_name]

    @folder_course = Article.find_by(profile: @tcc_community, name: course_name , parent_id: nil)
    @folder_tcc_submition = Article.where(parent: @folder_course).last

    @article = @parent = @folder_tcc_submition
    @target = @parent ? ('/%s/%s' % [@tcc_community.identifier, @parent.full_name]) : '/%s' % @tcc_community.identifier

  end

  def upload_files

    @uploaded_files = []

    tcc_community = Profile.find(params[:tcc_community].to_i)
    parent = Article.find(params[:parent].to_i)

    if request.post? && params[:uploaded_files]
      params[:uploaded_files].each do |file|
        unless file == ''
          @uploaded_files << UploadedFile.create(
          {
            :uploaded_data => file,
            :profile => tcc_community,
            :parent => parent,
            :last_changed_by => current_user.person,
            :author => current_user.person,
          },
          :without_protection => true
          )
        end
      end
      @errors = @uploaded_files.select { |f| f.errors.any? }
      if @errors.any?
        render :action => 'tcc_steps'
      else
        session[:notice] = _('File(s) successfully uploaded')

        if parent
          redirect_to "/tcc/#{parent.path}"
        else
          redirect_to :action => 'index'
        end
      end
    else
      session[:notice] = ('Não foi selecionado nenhum arquivo')
      render :action => 'tcc_steps'
    end
  end

end
